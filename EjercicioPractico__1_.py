#!/usr/bin/env python
# coding: utf-8

# # Actividad 4

# Ejemplo de un caso practico (Regresión logística).

# Estas son las líneas de código necesarias para la importación de estas librerías:

# In[49]:


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


# También es necesario importar los siguientes módulos de Keras:

# In[50]:


from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import SGD


# In[51]:


datos = pd.read_csv('dataset.csv', sep=",")


# In[52]:


X = datos.values[:,0:2]
Y = datos.values[:,2]


# In[53]:


idx0 = np.where(Y==0)
idx1 = np.where(Y==1)


# In[54]:


plt.scatter(X[idx0,0],X[idx0,1],color='red',label='Categoría: 0')
plt.scatter(X[idx1,0],X[idx1,1],color='gray',label='Categoría: 1')
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.legend(bbox_to_anchor=(0.765,0.6),fontsize=8,edgecolor='black')
plt.show()


# In[55]:


np.random.seed(1)
input_dim = X.shape[1]  # Dimensión: 2
output_dim = 1          # Dimensión: 1


# In[56]:


modelo = Sequential()
modelo.add(Dense(output_dim, input_dim = input_dim, activation='sigmoid'))


# In[57]:


sgd = SGD(learning_rate=0.2)
modelo.compile(loss='binary_crossentropy', optimizer=sgd, metrics=['accuracy'])


# In[58]:


modelo.summary()


# In[59]:


num_epochs = 1000
batch_size = X.shape[0]


# In[60]:


historia = modelo.fit(X, Y, epochs=num_epochs, batch_size=batch_size, verbose=2)


# In[61]:


plt.subplot(1,2,1)
plt.plot(historia.history['loss'])
plt.ylabel('Pérdida')
plt.xlabel('Epoch')
plt.title('Comportamiento de la pérdida')

plt.subplot(1,2,2)
plt.plot(historia.history['accuracy'])
plt.ylabel('Precisión')
plt.xlabel('Epoch')
plt.title('Comportamiento de la precisión')
plt.show()


# In[63]:


def dibujar_frontera(X,Y,modelo,titulo):
    # Valor mínimo y máximo y rellenado con ceros
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01

    # Grilla de puntos
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Predecir categorías para cada punto en la gruilla
    Z = modelo.predict_classes(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    fig = plt.figure()
    plt.contourf(xx, yy, Z, cmap=plt.cm.Set1, alpha=0.8)

    idx0 = np.where(Y==0)
    idx1 = np.where(Y==1)
    plt.scatter(X[idx0,0],X[idx0,1],color='red', edgecolor='k', label='Categoría: 0')
    plt.scatter(X[idx1,0],X[idx1,1],color='gray',edgecolor='k', label='Categoría: 1')
    plt.legend(bbox_to_anchor=(0.765,0.6),fontsize=8,edgecolor='black')

    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.title(titulo)

    plt.xlabel('$x_1$')
    plt.ylabel('$x_2$')
    plt.show()


# In[64]:





# In[ ]:




